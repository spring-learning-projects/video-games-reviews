<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<header class="d-flex justify-content-between">
	<h2 class="my-auto">Bienvenue ${sessionScope.joueur.pseudo} !</h2>
	<div class="text-right">
		<a href="deconnexion" class="btn btn-secondary btn-sm">Déconnexion</a>
	</div>
</header>