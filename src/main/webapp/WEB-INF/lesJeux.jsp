<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
		<title>Liste des jeux</title>
	    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
	    <link rel="stylesheet" href="css/style.css">
	</head>
	<body class="container my-3">
	
		<jsp:include page="componants/header.jsp"></jsp:include>
		
		<h3 class="my-2">Liste des jeux</h3>
		
		<table class="table table-striped table-bordered">
			<thead>
				<tr>
					<th>Nom<a href="?sort=nom,asc"class="dropdown-toggle"></a><a href="?sort=nom,desc"class="dropdown-toggle tourne"></a></th>
					<th>Editeur<a href="?sort=editeur.nom,asc"class="dropdown-toggle"></a><a href="?sort=editeur.nom,desc"class="dropdown-toggle tourne"></a></th>
					<th>Classification<a href="?sort=classification.nom,asc"class="dropdown-toggle"></a><a href="?sort=classification.nom,desc"class="dropdown-toggle tourne"></a></th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="jeu" items="${jeuxParPages.content}">
					<tr>
						<td class="align-middle">${jeu.nom}</td>
						<td class="align-middle">${jeu.editeur.nom}</td>
						<td class="align-middle">${jeu.classification.nom}</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		
		<a href="ajouter-jeu" class="btn btn-primary btn-sm">Ajouter un jeu</a>
		<a href="accueil" class="btn btn-primary btn-sm">Liste des avis</a>
		
		<nav aria-label="Page navigation example" class="my-3">
			<ul class="pagination">
				<c:if test="${!jeuxParPages.isFirst()}">
			    	<li class="page-item">
			     		<a class="page-link double" href="liste-jeux?page=0&sort=${sort}" aria-label="Previous">
			       	 		<span aria-hidden="true">&laquo;&laquo;</span>
			    		</a>
			    	</li>
			    	<li class="page-item">
			     		<a class="page-link" href="liste-jeux?page=${jeuxParPages.number-1}&sort=${sort}" aria-label="Previous">
			       	 		<span aria-hidden="true">&laquo;</span>
			    		</a>
			    	</li>
				</c:if>
		    	<li class="page-item"><a class="page-link" href="#">${jeuxParPages.getNumber()+1}</a></li>
		    	<c:if test="${!jeuxParPages.isLast()}">
			    	<li class="page-item">
			      		<a class="page-link" href="liste-jeux?page=${jeuxParPages.number+1}&sort=${sort}" aria-label="Next">
			      			<span aria-hidden="true">&raquo;</span>
			     		</a>
			  		</li>
			    	<li class="page-item">
			      		<a class="page-link double" href="liste-jeux?page=${jeuxParPages.getTotalPages() - 1}&sort=${sort}" aria-label="Next">
			      			<span aria-hidden="true">&raquo;&raquo;</span>
			     		</a>
			  		</li>
		    	</c:if>
		  	</ul>
		</nav>
		
		<c:if test="${jeuxParPages.numberOfElements ne 0}">
			<p>Jeux de ${jeuxParPages.size * jeuxParPages.number+1}
			 à ${(jeuxParPages.size * jeuxParPages.number+1) + jeuxParPages.numberOfElements-1} 
			 sur ${jeuxParPages.totalElements} jeux</p>
		</c:if>
		
	</body>
</html>