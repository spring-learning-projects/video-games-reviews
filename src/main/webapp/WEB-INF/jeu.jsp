<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Ajouter un jeu</title>
	    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
	    <link rel="stylesheet" href="css/style.css">
	</head>
	<body class="container my-3">
	
		<jsp:include page="componants/header.jsp"></jsp:include>
	
		<main>
			<h1>Ajout d'un jeu</h1>
			
			<form:form modelAttribute="jeu" action="ajouter-jeu" method="post">
				
				<div class="form-group">
					<form:label path="nom">Nom : </form:label>
					<form:input class="form-control" path="nom"/>
					<form:errors path="nom" cssClass="erreur" />
				</div>
				
				<div class="form-group">
					<form:label path="editeur">Editeur : </form:label>
					<form:select class="form-control" path="editeur">
						<form:option value="">Editeur</form:option>
						<form:options items="${editeurs}" itemValue="id" itemLabel="nom"/>
					</form:select>
					<form:errors path="editeur" cssClass="erreur" />
				</div>
				
				<div class="form-group">
					<form:label path="dateSortie">Date de sortie :</form:label>
					<form:input class="form-control" path="dateSortie" type="date"/>
					<form:errors path="dateSortie" cssClass="erreur" />
				</div>
				
				<div class="form-group">
					<form:label path="description">Description : </form:label>
					<form:input class="form-control" path="description"/>
					<form:errors path="description" cssClass="erreur" />
				</div>
				
				<div class="form-group">
					<form:label path="genre">Genre : </form:label>
					<form:select class="form-control" path="genre">
						<form:option value="">Genre</form:option>
						<form:options items="${genres}" itemValue="id" itemLabel="nom"/>
					</form:select>
					<form:errors path="genre" cssClass="erreur" />
				</div>
				
				<div class="form-group">
					<form:label path="classification">Classification : </form:label>
					<form:select class="form-control" path="classification">
						<form:option value="">Classification</form:option>
						<form:options items="${classifications}" itemValue="id" itemLabel="nom"/>
					</form:select>
					<form:errors path="classification" cssClass="erreur" />
				</div>
				
				<div class="form-group">
					<form:label path="plateformes">Plateforme : </form:label>
					<form:select class="form-control" path="plateformes">
						<form:options items="${plateformes}" itemValue="id" itemLabel="nom"/>
					</form:select>
					<form:errors path="plateformes" cssClass="erreur" />
				</div>
				
				<div class="form-group">
					<form:label path="modeleEconomique">Modele economique : </form:label>
					<form:select class="form-control" path="modeleEconomique">
						<form:option value="">Modele economique</form:option>
						<form:options items="${modelesEconomique}" itemValue="id" itemLabel="nom"/>
					</form:select>
					<form:errors path="modeleEconomique" cssClass="erreur" />
				</div>
				
				<form:button class="btn btn-success my-3">Ajouter le jeu</form:button>
			</form:form>
			
			<a href="accueil" class="btn btn-primary">Liste des avis</a>
		</main>
	</body>
</html>