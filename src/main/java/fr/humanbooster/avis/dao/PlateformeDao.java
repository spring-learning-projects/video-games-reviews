package fr.humanbooster.avis.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.humanbooster.avis.business.Plateforme;

public interface PlateformeDao extends JpaRepository<Plateforme, Long> {

}
