package fr.humanbooster.avis.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.humanbooster.avis.business.ModeleEconomique;

public interface ModeleEconomiqueDao extends JpaRepository<ModeleEconomique, Long> {

}
