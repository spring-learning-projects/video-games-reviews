package fr.humanbooster.avis.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.humanbooster.avis.business.Joueur;

public interface JoueurDao extends JpaRepository<Joueur, Long> {

	Joueur findByPseudoAndMotDePasse(String pseudo, String motDePasse);
}
