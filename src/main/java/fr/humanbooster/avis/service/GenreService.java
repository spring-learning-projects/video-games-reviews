package fr.humanbooster.avis.service;

import java.util.List;
import java.util.Optional;

import fr.humanbooster.avis.business.Genre;

public interface GenreService {

	Genre creerGenre(Genre genre);
	
	Optional<Genre> recupererGenre(Long id);
	
	List<Genre> recupererGenres();
	
	Genre majGenre(Genre genre);

	boolean supprimerGenre(Long id);
}
