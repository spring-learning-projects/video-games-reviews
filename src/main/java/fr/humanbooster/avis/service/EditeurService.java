package fr.humanbooster.avis.service;

import java.util.List;
import java.util.Optional;

import fr.humanbooster.avis.business.Editeur;

public interface EditeurService {

	Editeur creerEditeur(Editeur editeur);
	
	Optional<Editeur> recupererEditeur(Long id);
	
	List<Editeur> recupererEditeurs();
	
	Editeur majEditeur(Editeur editeur);

	boolean supprimerEditeur(Long id);
}
