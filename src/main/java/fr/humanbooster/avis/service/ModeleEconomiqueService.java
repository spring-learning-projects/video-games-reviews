package fr.humanbooster.avis.service;

import java.util.List;
import java.util.Optional;

import fr.humanbooster.avis.business.ModeleEconomique;

public interface ModeleEconomiqueService {

	ModeleEconomique creerModeleEconomique(ModeleEconomique modeleEconomique);
	
	Optional<ModeleEconomique> recupererModeleEconomique(Long id);
	
	List<ModeleEconomique> recupererModelesEconomique();
	
	ModeleEconomique majModeleEconomique(ModeleEconomique modeleEconomique);

	boolean supprimerModeleEconomique(Long id);
}
