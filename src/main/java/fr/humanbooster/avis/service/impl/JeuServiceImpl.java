package fr.humanbooster.avis.service.impl;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import fr.humanbooster.avis.business.Jeu;
import fr.humanbooster.avis.dao.JeuDao;
import fr.humanbooster.avis.service.JeuService;

@Service
public class JeuServiceImpl implements JeuService {

	@Autowired
	private JeuDao jeuDao;
	
	@Override
	public Jeu creerJeu(Jeu jeu) {
		return jeuDao.save(jeu);
	}

	@Override
	public Optional<Jeu> recupererJeu(Long id) {
		return jeuDao.findById(id);
	}

	@Override
	public List<Jeu> recupererJeux() {
		return jeuDao.findAll();
	}
	
	@Override
	public Page<Jeu> recupererJeux(Pageable pageable) {
		return jeuDao.findAll(pageable);
	}

	@Override
	public Jeu majJeu(Jeu jeu) {
		return jeuDao.save(jeu);
	}

	@Override
	public boolean supprimerJeu(Long id) {
		Jeu jeu = recupererJeu(id).get();
		if (jeu == null) {
			return false;
		} else {
			jeuDao.deleteById(id);
			return true;
		}
	}

	@Override
	public List<Jeu> recupererJeuxEntreDeuxDates(Date dateDebut, Date dateFin) {
		return jeuDao.findAllByDateSortieBetween(dateDebut, dateFin);
	}

}
