package fr.humanbooster.avis.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import fr.humanbooster.avis.business.Avis;
import fr.humanbooster.avis.business.Jeu;
import fr.humanbooster.avis.business.Joueur;
import fr.humanbooster.avis.dao.AvisDao;
import fr.humanbooster.avis.service.AvisService;

@Service
public class AvisServiceImpl implements AvisService {

	@Autowired
	private AvisDao avisDao;
	
	@Override
	public Avis creerAvis(Avis avis) {
		return avisDao.save(avis);
	}

	@Override
	public Optional<Avis> recupererAvis(Long id) {
		return avisDao.findById(id);
	}

	@Override
	public List<Avis> recupererAviss() {
		return avisDao.findAll();
	}
	
	@Override
	public Page<Avis> recupererAviss(Pageable pageable) {
		return avisDao.findAll(pageable);
	}

	@Override
	public Avis majAvis(Avis avis) {
		return avisDao.save(avis);
	}

	@Override
	public boolean supprimerAvis(Long id) {
		Avis avis = recupererAvis(id).get();
		if (avis == null) {
			return false;
		} else {
			avisDao.deleteById(id);
			return true;
		}
	}

	@Override
	public List<Avis> recupererAvissParJeu(Jeu jeu) {
		return avisDao.findAllByJeu(jeu);
	}
	
	@Override
	public long nombreTotalAvis() {
		return avisDao.count();
	}

	@Override
	public List<Avis> recupererAvisParJoueurOrdreDateEnvoiDesc(Joueur joueur) {
		return avisDao.findByJoueurOrderByDateEnvoiDesc(joueur);
	}

}
