package fr.humanbooster.avis.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.humanbooster.avis.business.Genre;
import fr.humanbooster.avis.dao.GenreDao;
import fr.humanbooster.avis.service.GenreService;

@Service
public class GenreServiceImpl implements GenreService {

	@Autowired
	private GenreDao genreDao;
	
	@Override
	public Genre creerGenre(Genre genre) {
		return genreDao.save(genre);
	}

	@Override
	public Optional<Genre> recupererGenre(Long id) {
		return genreDao.findById(id);
	}

	@Override
	public List<Genre> recupererGenres() {
		return genreDao.findAll();
	}

	@Override
	public Genre majGenre(Genre genre) {
		return genreDao.save(genre);
	}

	@Override
	public boolean supprimerGenre(Long id) {
		Genre genre = recupererGenre(id).get();
		if (genre == null) {
			return false;
		} else {
			genreDao.deleteById(id);
			return true;
		}
	}

}
