package fr.humanbooster.avis.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import fr.humanbooster.avis.business.Avis;
import fr.humanbooster.avis.business.Classification;
import fr.humanbooster.avis.business.Editeur;
import fr.humanbooster.avis.business.Jeu;
import fr.humanbooster.avis.business.Joueur;
import fr.humanbooster.avis.service.AvisService;
import fr.humanbooster.avis.service.ClassificationService;
import fr.humanbooster.avis.service.EditeurService;
import fr.humanbooster.avis.service.JeuService;
import fr.humanbooster.avis.service.JoueurService;

@RestController
@RequestMapping("/")
public class mainRestController {

	private EditeurService editeurService;
	private JoueurService joueurService;
	private AvisService avisService;
	private JeuService jeuService;
	private ClassificationService classificationService;
	
	private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

	public mainRestController(EditeurService editeurService, JoueurService joueurService, AvisService avisService,
			JeuService jeuService, ClassificationService classificationService) {
		super();
		this.editeurService = editeurService;
		this.joueurService = joueurService;
		this.avisService = avisService;
		this.jeuService = jeuService;
		this.classificationService = classificationService;
	}

	//==================== Question A ====================//
	/**
	 * Cette methode permet d'ajouter un editeur
	 * L'url devra contenir le nom de l'editeur
	 * @param nom
	 * @return
	 */
	@PostMapping(value = "/editeurs/{nom}", produces = "application/json")
	public Editeur creerEditeur(@PathVariable String nom){
	    return editeurService.creerEditeur(new Editeur(nom));
	}
	
	//==================== Question B ====================//
	/**
	 * Cette methode permet d'ajouter un à un joueur d'ajouter un avis
	 * L'url devra contenir l'id du joueur, l'id du jeu, la note ainsi que la description
	 * @param idJoueur
	 * @param idJeu
	 * @param note
	 * @param description
	 * @return
	 */
	@PostMapping(value = "/jeux/{idJoueur}/avis/{idJeu}/{note}/{description}", produces = "application/json")
	public Avis creerAvis(@PathVariable Long idJoueur, @PathVariable Long idJeu, @PathVariable Float note, @PathVariable String description){
		Joueur joueur = joueurService.recupererJoueur(idJoueur).get();
		Jeu jeu = jeuService.recupererJeu(idJeu).get();
		Avis avis = new Avis(new Date(), note, description, jeu, joueur);
	    return avisService.creerAvis(avis);
	}
	
	//==================== Question C ====================//
	/**
	 * Cette methode permet de recuperer tous les avis par jeu
	 * L'url devra contenir l'id du jeu
	 * @param idJeu
	 * @return
	 */
	@GetMapping(value = "/jeux/{idJeu}/avis", produces = "application/json")
	public List<Avis> recupererAvissParJeu(@PathVariable Long idJeu){
		Jeu jeu = jeuService.recupererJeu(idJeu).get();
	    return avisService.recupererAvissParJeu(jeu);
	}
	
	//==================== Question D ====================//
	/**
	 * Cette methode permet de changer le pseudo d'un joueur
	 * L'url devra contenir l'id du joueur et son nouveau pseudo
	 * @param idJoueur
	 * @param nouveauNom
	 * @return
	 */
	@PutMapping(value = "/jeux/{idJoueur}/{nouveauNom}", produces = "application/json")
	public Joueur modifierNomJoueur(@PathVariable Long idJoueur, @PathVariable String nouveauNom) {
		Joueur joueur = joueurService.recupererJoueur(idJoueur).get();
		joueur.setPseudo(nouveauNom);
		return joueurService.majJoueur(joueur);
	}
	
	//==================== Question E ====================//
	/**
	 * Cette methode permet d'ajouter une classification
	 * L'url devra contenir le nom de la nouvelle classification
	 * @param nom
	 * @return
	 */
	@PostMapping(value = "/classification/{nom}", produces = "application/json")
	public Classification creerClassification( @PathVariable String nom){
	    return classificationService.creerClassification(new Classification(nom));
	}
	
	//==================== Question F ====================//
	/**
	 * Cette methode permet de recuperer le joueur avec le plus d'avis
	 * @return
	 */
	@GetMapping(value = "/joueurs/top", produces = "application/json")
	public Joueur recupererJoueurPlusAvis(){
		Joueur joueurPlusAvis = joueurService.recupererJoueurs().get(0);
		for (Joueur joueur : joueurService.recupererJoueurs()) {
			if (joueur.getAvis().size() > joueurPlusAvis.getAvis().size()) {
				joueurPlusAvis = joueur;
			}
		}
		return joueurPlusAvis;
	}
	
	//==================== Question G ====================//
	/**
	 * Cette methode permet de supprimer un avis 
	 * L'url devra contenir l'id du jeu et l'id de l'avis
	 * @param idJeu
	 * @param idAvis
	 * @return
	 */
	@DeleteMapping(value = "/jeux/{idJeu}/avis/{idAvis}", produces = "application/json")
	public boolean supprimerAvis(@PathVariable Long idJeu, @PathVariable Long idAvis) {
		Jeu jeu = jeuService.recupererJeu(idJeu).get();
		Optional<Avis> avisASupprimer = avisService.recupererAvis(idAvis);
		if (avisASupprimer.isEmpty()) {
			return false;
		}
		for (Avis avis : jeu.getAvis()) {
			if (avis.equals(avisASupprimer.get())) {
				avisService.supprimerAvis(idAvis);
				return true;
			}
		}
		return false;
	}
	
	//==================== Question H ====================//
	/**
	 * Cette methode permet de recuperer le dernier avis d'un joueur
	 * L'url devra contenir l'id du joueur
	 * @param idJoueur
	 * @return
	 */
	@GetMapping(value = "/joueur/{idJoueur}/dernierAvis", produces = "application/json")
	public Avis dernierAvisJoueur(@PathVariable Long idJoueur) {
		Optional<Joueur> joueur = joueurService.recupererJoueur(idJoueur);
		if (joueur.isEmpty()) {
			return null;
		}
		return avisService.recupererAvisParJoueurOrdreDateEnvoiDesc(joueur.get()).get(0);
	}
	
	//==================== Question I ====================//
	@GetMapping(value = "/jeux", produces = "application/json")
	public List<Jeu> jeuEntreDeuxDates(@RequestParam("dateDebut") String dateDebut, @RequestParam("dateFin") String dateFin) {
		if (recupererDate(dateDebut) == null || recupererDate(dateFin) == null) {
			return null;
		}
		return jeuService.recupererJeuxEntreDeuxDates(recupererDate(dateDebut), recupererDate(dateFin));
	}
	
	//==================== Question I ====================//
	@GetMapping(value = "/jeux/top", produces = "application/json")
	/**
	 * Cette methode permet de recuperer le jeu le mieux notés
	 * @return
	 */
	public Jeu jeuMeilleureMoyenne() {
		
		Jeu meilleurJeu = null;
		
		for (Jeu jeu : jeuService.recupererJeux()) {

			float moyenne = 0;
			float meilleureNote = 0;
			
			for (Avis avis : jeu.getAvis()) {
				moyenne = moyenne + avis.getNote();
			}
			jeu.setNoteMoyenne(moyenne / jeu.getAvis().size());
			
			if (jeu.getNoteMoyenne() > meilleureNote) {
				meilleurJeu = jeu;
			}
			
		}

		return meilleurJeu;
	}
	
	
	//================== Méthode maison =================//
	private Date recupererDate(String dateString) {
        Date date = null;
        try {
            date = sdf.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }
	
}
